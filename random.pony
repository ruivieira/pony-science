use "random"
use "time"
use "collections"   

class Normal
    """
    Class to represent a Normal distribution
    """

    let _rand: Rand

    new create(rand: Rand) =>
        _rand = rand

    fun ref sample(mean: F64, std: F64): F64 =>
    """
    Sample a Normal deviate N(mean, std)
    """
        (normal_deviate() * std) + mean
    
    fun ref normal_deviate(): F64 =>
    """
    Sample a Normal deviate N(0,1)
    """
        var q : F64 = 10.0
        var v : F64 = 10.0
        var u : F64 = 0.0
        while (q > 0.27597) and ((q > 0.27846) or ((v * v) > (-4 * u.log() * u * u))) do
            u = _rand.real()
            v = 1.7156 * (_rand.real() - 0.5)
            let x = u - 0.449871
            let y = v.abs() + 0.386595
            q = (x * x) + (y * ((0.19600 * y) - (0.25472 * x)))
        end
        v / u


actor Main
    new create(env: Env) =>
        let time = Time.now()
        let rand = Rand(time._1.u64(), time._2.u64())
        let normal = Normal(rand)
        for i in Range(1, 100) do
            env.out.print(normal.sample(2.0, 2.0).string())
        end